#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2017 Yorick Buot de l'Epine

#
#Claptone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.

import numpy 
import os.path
import os
os.environ['ETS_TOOLKIT'] = 'qt5'
#import sip
#sip.setapi('QString', 2)
os.environ['QT_API'] = 'pyqt'
####################################
from PyQt5 import QtGui,QtCore
from PyQt5.QtCore import QObject, pyqtSignal
#from PyQt5.QtCore import SIGNAL
import pyqtgraph as pg
import pyaudio
import spectro_claptone_gui
import param_claptone_gui2
#import splashscreen
#import scipy.io.wavfile
import wave
#import sys
import psutil, os
#import time

def kill_proc_tree(pid, including_parent=True):    
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()


class plot_monitoring:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground((235,235,235))
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.courbe=self.p.plot()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setYRange(-1,1)
        self.p.setLabel('left', text ='<font size="4">Amplitude </font>', units ='<font size="4">Units </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Miliseconds </font>')
    def reset(self):
        self.p.clear()
        self.courbe=self.p.plot()          
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen('k',width=2),autoDownsample=True)      
         self.p.setYRange(-1.2*numpy.max(abs(Y)),1.2*numpy.max(abs(Y)))
         

class plot_2D_freq:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
#        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
        self.courbe=self.p.plot()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.setYRange(-120, 0)
        self.p.setXRange(0, 1)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='<font size="4">Amplitude </font>', units ='<font size="4">dB </font>')
        self.p.setLabel('bottom',text ='<font size="4">Frequency </font>', units ='<font size="4">Hertz </font>')
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem() 
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross()
    def reset(self):
        self.vLine.setValue(0)
        self.hLine.setValue(0)
        self.update_cross()
    def update_cross(self):
        x_max=0.9*(self.p.getViewBox().viewRange()[0][1])
        y_max=0.9*(self.p.getViewBox().viewRange()[1][0])
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=x
        ind_y=y
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y>0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>x_max:
            ind_x=x_max
            self.vLine.setValue(ind_x)
        if ind_y<y_max:
            ind_y=y_max
            self.hLine.setValue(ind_y)        
        self.vtext.setText(text="{0:.1f}".format(ind_x),color=(0,100,0))
        self.htext.setText(text="{0:.0f}".format(ind_y),color=(0,100,0))
        self.vtext.setPos(ind_x,0)
        self.htext.setPos(0,ind_y+20)
               
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen([25,12,112],width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])
         
class plot_2D_time:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
#        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
        self.courbe=self.p.plot()
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setYRange(-120, 0)
        self.p.setXRange(0,1)
        self.p.setLabel('left', text ='<font size="4">Amplitude </font>', units ='<font size="4">dB </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time</font>', units ='<font size="4">Seconds</font>')
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen((0,100,0),width=1))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem()
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross()
    def reset(self):
        self.vLine.setValue(0)
        self.hLine.setValue(0)
        self.update_cross()
    def update_cross(self):
        x_max=0.9*(self.p.getViewBox().viewRange()[0][1])
        y_max=0.9*(self.p.getViewBox().viewRange()[1][0])
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=x
        ind_y=y
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y>0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>x_max:
            ind_x=x_max
            self.vLine.setValue(ind_x)
        if ind_y<y_max:
            ind_y=y_max
            self.hLine.setValue(ind_y)        
        self.vtext.setText(text="{0:.1f}".format(ind_x),color=(0,100,0))
        self.htext.setText(text="{0:.0f}".format(ind_y),color=(0,100,0))
        self.vtext.setPos(ind_x,0)
        self.htext.setPos(0,ind_y+20)
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen([138,0,0],width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])

class plot_spectrum:
    def __init__(self,p,var):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        Xaxis.enableAutoSIPrefix(False)
        Xaxis.enableAutoSIPrefix(False)
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        self.p.setLabel('left', text ='<font size="4">Frequency </font>', units ='<font size="4">Hz </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Seconds </font>')     
        ##########################- define cross ###################
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen([25,12,112],width=2))
        self.vLine.setHoverPen(pg.mkPen('w',width=2))
        self.hLine = pg.InfiniteLine(angle=0, movable=True,pen=pg.mkPen([138,0,0],width=2))
        self.hLine.setHoverPen(pg.mkPen('w',width=2))
        self.vtext=pg.TextItem() 
        self.htext=pg.TextItem()
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.hLine, ignoreBounds=True)
        self.p.addItem(self.vtext)
        self.p.addItem(self.htext)
        font=QtGui.QFont('AnyStyle',pointSize=9,weight=99)
        self.htext.setFont(font)
        self.vtext.setFont(font)
        self.update_cross_by_label(var)
        
    def reset(self,var):
        self.p.removeItem(self.image)
        self.p.removeItem(self.p.getPlotItem().getAxis('bottom'))
        self.p.removeItem(self.p.getPlotItem().getAxis('left'))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        ##########################- define cross ###################
        self.image.setZValue(-1) #◙remet l'image à l'arrière plan
        self.update_cross_by_label(var)
    
    def update_cross(self,var):
        x=self.vLine.value()
        y=self.hLine.value()
        ind_x=int(round(x))
        ind_y=int(round(y))
        if ind_x<0:
            ind_x=0
            self.vLine.setValue(ind_x)
        if ind_y<0:
            ind_y=0
            self.hLine.setValue(ind_y)
        if ind_x>numpy.size(var.data_time):
            ind_x=numpy.size(var.data_time)-10
            self.vLine.setValue(ind_x)
        if ind_y>numpy.size(var.data_freq):
            ind_y=numpy.size(var.data_freq)-2
            self.hLine.setValue(ind_y)
        slide_time_ind=ind_x
        slide_time=var.data_time[ind_x]
        self.vtext.setText(text="{0:.1f}".format(var.slide_time),color=[25,12,112])
        self.vtext.setPos(ind_x,2)
        slide_freq_ind=ind_y
        slide_freq=var.data_freq[ind_y]
        self.htext.setText(text="{0:.0f}".format(var.slide_freq),color=[138,0,0])
        self.htext.setPos(numpy.size(var.data_time)-20,var.slide_freq_ind)
        return (slide_time,slide_time_ind,slide_freq,slide_freq_ind)
    
    def update_cross_by_label(self,var):
        self.vLine.setValue(var.slide_time_ind)
        self.hLine.setValue(var.slide_freq_ind)          
        self.vtext.setText(text="{0:.1f}".format(var.slide_time),color=[25,12,112])
        self.htext.setText(text="{0:.0f}".format(var.slide_freq),color=[138,0,0])
        self.vtext.setPos(var.slide_time_ind,2)
        self.htext.setPos(numpy.size(var.data_time)-20,var.slide_freq_ind)
        
    def update_data(self,mat,db_min,db_max):          
         self.image.setImage(mat,autoDownSample=True)  
         self.image.setLevels([db_min,db_max])       

class Variable:
    def __init__(self):
        self.freq_max = [] #max frequency to study
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed
        self.channel=[] # voie
        self.time_range=[] 
        self.time_max=[]
        self.slide_freq=0
        self.slide_freq_ind=0
        self.slide_time=0
        self.slide_time_ind=0        
        self.data_time=[]
        self.data_freq=[]
        self.nb_cover=[]
        self.db_min=[]
        self.db_max=[]
        self.factor_rate=2.0
        self.sensi=[]
        self.ref=[]

class signal_BUFF:
    def init(self,signal_size):
        self.data=numpy.zeros((signal_size))
    def add(self,x):
        self.data[0:numpy.size(x)]=x
        self.data=numpy.roll(self.data,-numpy.size(x))

class spectro_BUFF:
    def init(self,spectro_size):
        self.data=1e-7*numpy.ones((spectro_size),'complex')
    def add(self,x,n):
        self.data[:,0:n]=x
        self.data=numpy.roll(self.data,-n)
    def abs_data(self):
        return numpy.abs(self.data)

def fenetre(weight_hanning,bloc):
    bloc_out=numpy.multiply(weight_hanning,bloc)
    return bloc_out    
    
def define_scope_index(time_data,time_range):
    time_range=time_range/float(1000)
    if time_range>time_data[-1]:
            index=numpy.arange(numpy.size(time_data))
    else:
        index=[]
        midtime=time_data[int(numpy.size(time_data)/float(2))]
        for ii,ee in enumerate(time_data):
            if numpy.abs(ee-midtime)<time_range/2:
                index.append(ii)
    return index    
    
    ############Î definie la table des couleurs
def cubehelix(gamma=1, s=0.5, r=-1.5, h=1.0):
    def get_color_function(p0, p1):
        def color(x):
            xg = x ** gamma
            a = h * xg * (1 - xg) / 2
            phi = 2 * numpy.pi * (s / 3 + r * x)
            return xg + a * (p0 * numpy.cos(phi) + p1 * numpy.sin(phi))
        return color
    array = numpy.empty((256, 3))
    abytes = numpy.arange(0, 1, 1/256.)
    array[:, 0] = get_color_function(-0.14861, 1.78277)(abytes) * 255
    array[:, 1] = get_color_function(-0.29227, -0.90649)(abytes) * 255
    array[:, 2] = get_color_function(1.97294, 0.0)(abytes) * 255
    return array

def rainbow():
    lut = numpy.empty((256, 3))
    abytes = numpy.arange(0, 1, 0.00390625)
    lut[:, 0] = numpy.abs(2 * abytes - 0.5) * 255
    lut[:, 1] = numpy.sin(abytes * numpy.pi) * 255
    lut[:, 2] = numpy.cos(abytes * numpy.pi / 2) * 255
    return lut
    
def color_table():
    pos = numpy.array([0.0, 0.5, 1.0])
    color = numpy.array([[0,0,0,255], [255,128,0,255], [255,255,0,255]], dtype=numpy.ubyte)
    map = pg.ColorMap(pos, color)
    lut = map.getLookupTable(0.0, 1.0, 256)
    return lut

def colormap_gui(fig,min_val,max_val):
    pen = pg.mkPen(color='k', width=2)
    nb_color=255
    value=numpy.array([numpy.linspace(min_val,max_val,nb_color),numpy.linspace(min_val,max_val,nb_color)])
    colortable=cubehelix()
#    colortable=rainbow()
   # colortable=color_table()
    fig.clear()       
    fig.setBackground((235.0,235.0,235.0))
    fig.plotItem.getAxis('left').setPen(pen)
    fig.plotItem.getAxis('bottom').setPen(pen)
    image=pg.ImageItem()
    item_color=fig.getPlotItem()
    image.setLookupTable(colortable)
    fig.addItem(image)
    image.setImage(value,autoDownSample=True) 
    image.setLevels([min_val,max_val])
    Xaxis=item_color.getAxis('bottom')
    Yaxis=item_color.getAxis('left')
    ntick=7
    Ytick_loc=[]
    Ytick_string=[]
    for ii in range(0,ntick+1):
        Ytick_string.append("{0:.1f}".format(round(min_val+(ii*((max_val-min_val)/float(ntick))))))
        Ytick_loc.append(round((ii*((nb_color)/float(ntick)))))
    Ytick=zip(Ytick_loc,Ytick_string)
    Yaxis.setTicks([list(Ytick)])
    Xaxis.hide()
    
def write_spectro(fid,data,freq,time):
    shape=numpy.shape(data)
    for ii in range(0,shape[1]):
        fid.write("{0:.3f}".format(time[ii])+';')
    fid.write('\n')
    for jj in range(0,shape[0]):
        fid.write("{0:.3f}".format(freq[jj])+';')
        for ii in range(0,shape[1]):
            fid.write("{0:.3f}".format(data[jj,ii])+';')
        fid.write('\n')
#    fid.close()
    
class splashScreen:
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash_claptone.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)
        
    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()
           
        
        
class param_window(QtGui.QWidget,param_claptone_gui2.Ui_param):
    def __init__(self, parent=None):
        super(param_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Parameter")     


class spectro(QtGui.QMainWindow,spectro_claptone_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(spectro, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"ClapTone") 
        ############### definie la fenetre des paramètre ########
        self.param = param_window()
        ############# initalise variable ###################
        os.chdir('C:\\')
        self.STOP_main=0
        self.variable=Variable()
        self.time_box.addItems(['200','100','50','20','10','5'])
        self.time_box.setCurrentIndex(3)
        self.variable.time_range=float(self.time_box.currentText()) # résolution temporel 
        self.time_record_box.addItems(['1','2','5','10'])
        self.time_record_box.setCurrentIndex(3)
        self.variable.time_max=float(self.time_record_box.currentText()) # résolution temporel 
        self.param.voie_box.addItems(['Left','Right'])
        self.param.voie_box.setCurrentIndex(0)
        self.define_channel()
        self.param.freq_edit.insert('4000')
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.param.line_box.addItems(['128','256','512','1024','2048','4096'])
        self.param.line_box.setCurrentIndex(0)
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.param.fenetre_box.addItems(['Uniform','Hanning'])
        self.param.fenetre_box.setCurrentIndex(1)
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.param.recouvrement_edit.insert('0.66')
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.param.sensi_edit.insert('1000')
        self.variable.sensi=1e-3*float(self.param.sensi_edit.text())
        self.param.ref_edit.insert('1')
        self.variable.ref=float(self.param.ref_edit.text())
        self.define_delta_f() # définie le delta f
        self.define_data_freq_time()        
        #############################################
        self.min_db_edit.insert('-80')
        self.max_db_edit.insert('0')
        self.actu_time_range()
        ##############################################        
        ########## init fig ############################################################
        self.freq_fft_plot=plot_2D_freq(self.plot_freq_fft)
        self.time_fft_plot=plot_2D_time(self.plot_time_fft)
        self.time_plot=plot_monitoring(self.plot_time)
        self.spectro_plot=plot_spectrum(self.plot_spectro,self.variable)
        #################################################
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.freq_max/2))
        self.time_slide_edit.insert("{0:.1f}".format(self.variable.time_max/2))
        self.select_freq_slide()
        self.select_time_slide()
        self.define_colormap()
        ################################################### 
        
        #################################################☺
        self.connectActions()
         
    def connectActions(self):
        ######################################
        self.param.enregistrement_button.clicked.connect(self.save_param)
        self.param.freq_edit.editingFinished.connect(self.define_delta_f)
        self.param.line_box.currentIndexChanged.connect(self.define_delta_f)
        self.param.fenetre_box.currentIndexChanged.connect(self.define_window)
        #######################################
        self.option_button.clicked.connect(self.param_open)
        self.stop_button.clicked.connect(self.main_stop)
        self.start_button.clicked.connect(self.main)
        self.listen_button.clicked.connect(self.listen_buffer)
        self.wave_button.clicked.connect(self.save_wave)
        self.time_box.activated.connect(self.actu_time_range)
        self.time_record_box.activated.connect(self.actu_time_max)
        self.freq_slide_edit.editingFinished.connect(self.select_freq_slide)
        self.time_slide_edit.editingFinished.connect(self.select_time_slide)
        self.max_db_edit.editingFinished.connect(self.define_colormap)
        self.min_db_edit.editingFinished.connect(self.define_colormap)
        self.temp_export_button.clicked.connect(self.export_time_FFT)
        self.freq_export_button.clicked.connect(self.export_freq_FFT)
        self.spectro_export_button.clicked.connect(self.export_spectro_FFT)
        self.freq_fft_plot.hLine.sigDragged.connect(self.freq_fft_plot.update_cross)
        self.freq_fft_plot.vLine.sigDragged.connect(self.freq_fft_plot.update_cross)
        self.time_fft_plot.hLine.sigDragged.connect(self.time_fft_plot.update_cross)
        self.time_fft_plot.vLine.sigDragged.connect(self.time_fft_plot.update_cross)
#        self.variable.slide_time,self.variable.slide_time_ind,self.variable.slide_freq,self.variable.slide_freq_ind=self.spectro_plot.hLine.sigDragged.connect(lambda: self.spectro_plot.update_cross(self.variable))
#        self.variable.slide_time,self.variable.slide_time_ind,self.variable.slide_freq,self.variable.slide_freq_ind=self.spectro_plot.vLine.sigDragged.connect(lambda: self.spectro_plot.update_cross(self.variable))
        self.spectro_plot.hLine.sigDragged.connect(self.update_cross_spectro)
        self.spectro_plot.vLine.sigDragged.connect(self.update_cross_spectro)
        
    def param_open(self):
        """Lance la deuxième fenêtre"""
        self.param.show() 
        self.main_stop()
        
    def save_param(self):
        self.define_channel()
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_data_freq_time()
        self.variable.sensi=1e-3*float(self.param.sensi_edit.text())
        self.variable.ref=float(self.param.ref_edit.text())
#        print(self.variable.ref)
        ########♣ remet les slide au milieu des plage de frequence et de temps
        self.freq_slide_edit.clear()
        self.time_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.freq_max/2.0))
        self.time_slide_edit.insert("{0:.1f}".format(self.variable.time_max/2.0))
        self.select_freq_slide()
        self.select_time_slide() 
        self.param.close()
#        disp(self.signal_buffer.data)
#        disp(self.spectro_buff.data)
##        disp(self.variable.buffer_size)
##        disp(self.variable.freq_max)
##        disp(self.variable.data_freq)
##        disp(self.variable.data_time)
        #######################################
        
    def define_data_freq_time(self):
        delta_f=self.variable.delta_freq# equivalant au nombre de buffer à remplir par seconde
        self.variable.nb_cover=int(round(1/(1-self.variable.covering)))
        self.variable.data_time=numpy.linspace(0,self.variable.time_max,
                                               round(self.variable.nb_cover*self.variable.time_max*delta_f))
        self.variable.data_freq=numpy.arange(0,self.variable.freq_max,delta_f)#[0:-1]
        ################## créer les buffers ##############################################
        ###### Signal audio pour toute la duree de l'enregistrement #####
        self.signal_buffer=signal_BUFF()
        self.signal_buffer.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        ####################### spectre sur toute la durrée de l'enregistrement ###########
        self.spectro_buff=spectro_BUFF()
        self.spectro_buff.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 

    def define_channel(self):
        voie=self.param.voie_box.currentText()
        if voie=='Left':
            self.variable.channel=0
        if voie=='Right':
            self.variable.channel=1
    
    def define_window(self):
        fenetre=self.param.fenetre_box.currentText()
        if fenetre=='Uniform':
            self.param.recouvrement_edit.setText('0')
        if fenetre=='Hanning':
            self.param.recouvrement_edit.setText('0.66')
               
    def define_delta_f(self):
        self.param.enregistrement_button.setEnabled(True)
        freq_max=int(self.param.freq_edit.text())
        ####################################################################
        if freq_max>=500 and freq_max<=9000:
            buffer =self.variable.factor_rate*int(self.param.line_box.currentText()) # size of buffer
            self.param.enregistrement_button.setEnabled(True)
            self.variable.delta_freq=int(self.variable.factor_rate*freq_max)/float(buffer)
            self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))
        if freq_max>9000:
            buffer =self.variable.factor_rate*int(self.param.line_box.currentText()) # size of buffer
            if buffer<1024:
                self.param.delta_label.setText('FMax>9000Hz-->FFT line>=400')
                self.param.enregistrement_button.setEnabled(False)
            else:
                self.variable.delta_freq=int(self.variable.factor_rate*freq_max)/float(buffer)
                self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))
        if freq_max>22000:
            self.param.freq_edit.clear()
            self.param.delta_label.setText('Freq Max-[500Hz,122000Hz]')
            self.param.enregistrement_button.setEnabled(False)
        if freq_max<500:
            self.param.freq_edit.clear()
            self.param.delta_label.setText('Freq Max-[500Hz,22000Hz]')
            self.param.enregistrement_button.setEnabled(False)
    
    def define_colormap(self):
        val_min=float(self.min_db_edit.text())
        val_max=float(self.max_db_edit.text())
        colormap_gui(self.plot_colorbar,val_min,val_max)
        self.variable.db_min=val_min
        self.variable.db_max=val_max
        ############# si pas d'acquisition, on replote le spectre du buffer #####
        if self.STOP_main==1:
            self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buff.data/self.variable.ref).T),self.variable.db_min,self.variable.db_max)

    def export_time_FFT(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_time_fft.txt',filter ='txt (*.txt *.)')
        try:
            fid=open(filename[0],'w')
            line='Amplitude vs Time    '+'Freq='+"{0:.1f}".format(self.variable.slide_freq)+'Hz \n'
            fid.write(line)
            fid.write('Time      Decibels\n')
            for ii in range(0,numpy.size(self.variable.data_time)):
                fid.write("{0:.6f}".format(self.variable.data_time[ii])+';'+"{0:.3f}".format(20*numpy.log10(abs(self.spectro_buff.data[self.variable.slide_freq_ind,ii])/self.variable.ref))+'\n')
            fid.close()
        except:
            pass

    def export_freq_FFT(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_freq_fft.txt',filter ='txt (*.txt *.)')
        try:
            fid=open(filename[0],'w')
            line='Amplitude vs Freq     '+'Time='+"{0:.3f}".format(self.variable.slide_time)+'s \n'
            fid.write(line)
            fid.write('Freq      Decibels\n')
            for ii in range(0,numpy.size(self.variable.data_freq)):
                fid.write("{0:.3f}".format(self.variable.data_freq[ii])+';'+"{0:.3f}".format(20*numpy.log10(abs(self.spectro_buff.data[ii,self.variable.slide_time_ind])/self.variable.ref))+'\n')        
            fid.close()
        except:
            pass

    def export_spectro_FFT(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_spectro_fft.txt',filter ='txt (*.txt *.)')
        try:
            fid=open(filename[0],'w')
            line='Freq (Freq values:line 1) vs Time (Time values:row 1)\n'
            fid.write(line)
            write_spectro(fid,20*numpy.log10(abs(self.spectro_buff.data/self.variable.ref)),self.variable.data_freq,self.variable.data_time)
            fid.close()
        except:
            pass
        
        
    def select_freq_slide(self):
        freq_search=float(self.freq_slide_edit.text())
        freq_list=abs(self.variable.data_freq-numpy.array([freq_search]*len(self.variable.data_freq)))
        ind=numpy.where(freq_list==min(freq_list))[0]
        self.variable.slide_freq_ind=ind[0]
        self.variable.slide_freq=self.variable.data_freq[self.variable.slide_freq_ind]
        self.freq_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.slide_freq))
        self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
        ############# si pas d'acquisition, on plot le résultats déjà enegistré
        if self.STOP_main==1:
            self.time_fft_plot.update_data(self.variable.data_time,20*numpy.log10(abs(self.spectro_buff.data[self.variable.slide_freq_ind,:]/self.variable.ref)))

    def select_time_slide(self):
        time_search=float(self.time_slide_edit.text())
        time_list=abs(self.variable.data_time-numpy.array([time_search]*len(self.variable.data_time)))
        ind=numpy.where(time_list==min(time_list))[0]
        self.variable.slide_time_ind=ind[0]
        self.variable.slide_time=self.variable.data_time[self.variable.slide_time_ind]
        self.time_slide_edit.clear()
        self.time_slide_edit.insert("{0:.1f}".format(self.variable.slide_time))
        self.spectro_plot.update_cross_by_label(self.variable)# replor le spectre avec les lignes de reference
        ################## si pas d'acquisition, on plot les resulats enregistré
        if self.STOP_main==1:
            ###########################
            self.freq_fft_plot.update_data(self.variable.data_freq,20*numpy.log10(abs(self.spectro_buff.data[:,self.variable.slide_time_ind]/self.variable.ref)))
            self.spectro_plot.update_cross(self.variable)# replot le spectre avec les lignes de reference
    
    def update_cross_spectro(self):
        time,time_ind,freq,freq_ind=self.spectro_plot.update_cross(self.variable)
        self.variable.slide_time=time
        self.variable.slide_time_ind=time_ind
        self.variable.slide_freq=freq
        self.variable.slide_freq_ind=freq_ind
        ##########################################
        self.freq_slide_edit.clear()
        self.freq_slide_edit.insert("{0:.1f}".format(self.variable.slide_freq))
        self.time_slide_edit.clear()
        self.time_slide_edit.insert("{0:.2f}".format(self.variable.slide_time))     
        if self.STOP_main==1:
            self.time_fft_plot.update_data(self.variable.data_time,20*numpy.log10(abs(self.spectro_buff.data[self.variable.slide_freq_ind,:]/self.variable.ref)))
            self.freq_fft_plot.update_data(self.variable.data_freq,20*numpy.log10(abs(self.spectro_buff.data[:,self.variable.slide_time_ind]/self.variable.ref)))
        
    def actu_time_range(self):
        self.variable.time_range=int(self.time_box.currentText()) 
        time_sig=numpy.arange(self.variable.buffer_size)/float(int(self.variable.factor_rate*self.variable.freq_max))
        self.index_signal=define_scope_index(time_sig,self.variable.time_range)
        
    def actu_time_max(self):
        self.main_stop()
        self.variable.time_max=int(self.time_record_box.currentText())
        self.save_param()# a changer plus tard fonctionne pour le moment

    
    def init_time_range(self,list_time,defaut):
        """
        Initialise la liste des temps de résolution
        """
        self.time_box.addItems(list_time)
        self.time_box.setCurrentIndex(defaut)

    def main(self):
        ############# intialisation du GUI
        self.STOP_main=0
        self.start_button.setEnabled(False)
        self.stop_button.setEnabled(True)
        self.temp_export_button.setEnabled(False)
        self.freq_export_button.setEnabled(False)
        self.spectro_export_button.setEnabled(False)
        self.wave_button.setEnabled(False)
        self.listen_button.setEnabled(False)
        ########## intialisation des paramètre ########
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        BUFFER=self.variable.buffer_size
        if self.variable.window=='Hanning':
            weight=0.5-0.5*(numpy.cos(2*numpy.pi*numpy.arange(0,BUFFER)/BUFFER))
        if self.variable.window=='Uniform':
            weight=numpy.ones((self.variable.buffer_size))
        taux_overlap=self.variable.covering
        nb_cover=int(round(1/(1-taux_overlap)))
        sensi=self.variable.sensi # on recupere la sensibilite
        ref=self.variable.ref # reference ppour le decibels
        ######### determination lignes frequentielles et temproelles ######
        time_data=self.variable.data_time# ligne temporelle spectrogramme (prise en compte du recouvrement)
        freq_data=self.variable.data_freq# ligne freq spectrograme
        time_signal=numpy.arange(BUFFER)/float(RATE)
        time_signal=(time_signal-(time_signal[-1]/2))*1000# ligne temporelles signal tempo (plot)
        # taux de raffracihissement  à améliorer
        if self.variable.time_max<10:
            inc_raffraich=2
        else:
            inc_raffraich=4# n'existe plus
        ######### intialisation des ringbuffer et matrice vide #############
        signal=1e-7*numpy.ones((BUFFER))
        data_fft=1e-7*numpy.ones((BUFFER,nb_cover),'complex')
        ind=numpy.zeros((BUFFER,nb_cover),dtype=int)
        for j in range(0,nb_cover):   
            ind[:,j]= numpy.arange(j*(1-taux_overlap)*BUFFER,j*(1-taux_overlap)*BUFFER+BUFFER).astype(int)
        ########### intialisation des figures #############
        self.time_plot.reset()
        self.time_plot.update_data(time_signal[self.index_signal],signal[self.index_signal])
        ###################
        self.freq_fft_plot.reset()
        self.freq_fft_plot.update_data(self.variable.data_freq,20*numpy.log10(abs(self.spectro_buff.data[:,self.variable.slide_time_ind]))/float(ref))
        ###########################
        self.time_fft_plot.reset()
        self.time_fft_plot.update_data(self.variable.data_time,20*numpy.log10(abs(self.spectro_buff.data[self.variable.slide_freq_ind,:]))/float(ref))
        ############################
        self.spectro_plot.reset(self.variable)
        self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buff.data).T/float(ref)),self.variable.db_min,self.variable.db_max)
#        self.spectro_plot.update_cross_by_label(self.variable.slide_time,self.variable.slide_freq,self.variable.slide_time_ind,self.variable.slide_freq_ind)# replor le spectre avec les lignes de reference
        ###############################################
        app.processEvents()
        ######## initalisation de la classe pyaudio ######
        p=pyaudio.PyAudio()
#        info = p.get_host_api_info_by_index(0)
#        numdevices = info.get('deviceCount')
##for each audio device, determine if is an input or an output and add it to the appropriate list and dictionary
#        for i in range (0,numdevices):
#            if p.get_device_info_by_host_api_device_index(0,i).get('maxInputChannels')>0:
#                    print "Input Device id ", i, " - ", p.get_device_info_by_host_api_device_index(0,i).get('name')
#                    
#            if p.get_device_info_by_host_api_device_index(0,i).get('maxOutputChannels')>0:
#                print "Output Device id ", i, " - ", p.get_device_info_by_host_api_device_index(0,i).get('name')

        devinfo = p.get_device_info_by_index(1)
        nb_input=min(devinfo['maxInputChannels'],2)
        if nb_input==1:
            self.variable.channel=0
        stream = p.open(format=pyaudio.paFloat32,channels=nb_input,rate=RATE,input=True,frames_per_buffer=BUFFER)
        ####### lanccement de l'acquisition #########          
        k=0
#        start_time=time.time()  
        while self.STOP_main==0:
#        stop=round(numpy.size(self.variable.data_time)/3)
#        print(stop)
#        for jj in range(0,int(stop)):
            k=k+1
            data=stream.read(BUFFER)#enregistre les donnée du buffer 
            signal=(1/float(sensi))*numpy.reshape(numpy.frombuffer(data, 'float32'),(BUFFER,nb_input))[:,self.variable.channel]# division par la sensibilite pour avoir la donnée en unite
            self.signal_buffer.add(signal)
            signal_fft_data=self.signal_buffer.data[-2*self.variable.buffer_size:]
            for j in range(0,self.variable.nb_cover):
                windowed_signal=fenetre(weight,signal_fft_data[ind[:,j]])
                data_fft[:,j]=numpy.fft.fft(windowed_signal,BUFFER)/(BUFFER)
            self.spectro_buff.add(2*data_fft[0:numpy.size(freq_data),:],nb_cover)
            self.spectro_buff.data[self.spectro_buff.data==0]=1e-7#evite les cas infinie avec le logarithme
            self.spectro_buff.data[-1,:]=1e-7#evite l'artefact sur la dernière ligne
            self.time_plot.update_data(time_signal[self.index_signal],signal[self.index_signal])
            if numpy.mod(k,inc_raffraich)==0:
                k=0
                data_plot=abs(self.spectro_buff.data)
                self.spectro_plot.update_data(20*numpy.log10(data_plot.T/float(ref)),self.variable.db_min,self.variable.db_max)
                #########################################################################                
                self.freq_fft_plot.update_data(freq_data,20*numpy.log10(data_plot[:,self.variable.slide_time_ind]/float(ref)))
                self.time_fft_plot.update_data(time_data,20*numpy.log10(data_plot[self.variable.slide_freq_ind,:]/float(ref)))
                ##########################################################################
                app.processEvents()
#        end_time=time.time()
#        print(end_time-start_time)
        stream.stop_stream()
        stream.close()
        p.terminate() 
        self.start_button.setEnabled(True)
    
    def listen_buffer(self):
        self.listen_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        BUFFER=self.variable.buffer_size
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels= 1,rate=RATE,input=False,output=True,frames_per_buffer=BUFFER)
        data=self.signal_buffer.data
        stream.write(data.astype(numpy.float32).tostring())
        stream.close()
        p.terminate()
#        QtCore.QThread.sleep(self.variable.time_max)
        self.listen_button.setEnabled(True)
        app.processEvents()
        
        
    def save_wave(self):
        data=self.signal_buffer.data
        data2=numpy.multiply(data,32767).astype(numpy.int16)
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','Claptone.wav',filter ='wav (*.wav *.)')
        try:
            wave_file = wave.open(filename[0], 'w')
            wave_file.setparams((1, 2, RATE, 0, 'NONE', 'not compressed'))        
            for ii in range(numpy.size(data2)):
                signal= wave.struct.pack('<h', int(data2[ii]))
                wave_file.writeframesraw( signal )
#        scipy.io.wavfile.write(filename, RATE, data2)
            wave_file.close
        except:
            pass
        self.wave_button.setEnabled(True)
        
    def main_stop(self):
        self.STOP_main=1
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(False)
        self.start_button.setEnabled(True)
        self.temp_export_button.setEnabled(True)
        self.freq_export_button.setEnabled(True)
        self.spectro_export_button.setEnabled(True)
        self.wave_button.setEnabled(True)
        self.listen_button.setEnabled(True)
        
if __name__=='__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()  
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    gui = spectro()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-  
    me = os.getpid()
    kill_proc_tree(me)
    